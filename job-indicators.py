#!/usr/bin/python
#TODO check the directory to create the temporal file
#from xml.dom import minidom
import xml.etree.ElementTree as ET
import fileinput,os,itertools,re

#Variables
filename = "./job_logs/20140129"

#Create a temporal file
temporalFileName = filename + ".tmp" 
os.system("cp "+filename+" "+temporalFileName)

#Replace the </JobId> tag for the correct </Job_Id> tag
for line in fileinput.FileInput(temporalFileName,inplace=1):
    	print line.replace("JobId","Job_Id")

#Insert a <root> tag at the beginning of the temporal file
os.system("echo \"<root>\n$(cat " + temporalFileName + ")\" > " + temporalFileName)
#Insert a </root> tag at the end of the temporal file
os.system("echo \"</root>\" >> " + temporalFileName)

#Parse xml ---------------------
tree = ET.parse(temporalFileName)
root = tree.getroot()

#Statistics to be stored
clusterNumberOfCompletedJobs = 0
clusterTotalComputedTime = 0
clusterAveragetimePerWork = 0
clusterTotalMemoryUsage = 0
clusterTotalVirtualMemoryUsage = 0
clusterAverageMemoryUsage = 0
clusterAverageVirtualMemoryUsage = 0

#Extract the required values
for child in root:
	#print child.tag
	#print child.text
	#Find the job state
	#print child.iter
	
	for state in child.findall('job_state'):
		job_state = state.text 
		
	#process according to the state
	#if the state is: "completed"
	if(job_state == "C"):
		jobId = ""
		user =  ""
		group = ""
		queue = ""
		startTime = 0
		completionTime = 0
		queuedTime = 0
		creationTime = 0
		memoryUsage = 0
		virtualMemoryUsage = 0

		totalRuningTime = 0
		totalQueuedTime = 0
		totalTime = 0

		for subchild in child:

			tag = subchild.tag
			tagText = subchild.text

			if(tag == "euser"):
				user = tagText

			elif(tag == "egroup"):
				group = tagText

			elif(tag == "queue"):
				queue = tagText

			elif(tag == "Job_Id"):
				jobId = tagText

			elif(tag == "start_time"):
				startTime = int(tagText)

			elif(tag == "comp_time"):
				completionTime = int(tagText)

			elif(tag == "ctime"):
				creationTime = int(tagText)

			elif(tag == "etime"):
				queuedtime = int(tagText)

			elif(tag == "resources_used"):
				for subsubchild in subchild:
					tag = subsubchild.tag
					tagText = subsubchild.text
					if(tag == "mem"):
						memoryUsage = int(re.sub("[^0-9]", "", tagText))
					elif(tag == "vmem"):
						virtualMemoryUsage = int(re.sub("[^0-9]", "", tagText))

		totalRuningTime = completionTime - startTime
		totalQueuedTime = startTime - queuedtime
		totalTime = completionTime - creationTime

		clusterTotalComputedTime += totalRuningTime 
		clusterTotalMemoryUsage += memoryUsage
		clusterTotalVirtualMemoryUsage += virtualMemoryUsage
		clusterNumberOfCompletedJobs += 1 
		
		'''
		print jobId
		print user
		print group
		print queue
		print totalRuningTime
		print totalQueuedTime
		print totalTime
		print memoryUsage
		print virtualMemoryUsage
		print "------------------------------"
		'''

clusterAveragetimePerWork = clusterTotalComputedTime/clusterNumberOfCompletedJobs
clusterAverageMemoryUsage = clusterTotalMemoryUsage/clusterNumberOfCompletedJobs
clusterAverageVirtualMemoryUsage = clusterTotalVirtualMemoryUsage/clusterNumberOfCompletedJobs

print "Number of completed jobs: ",clusterNumberOfCompletedJobs
print "Total computed time: ",clusterTotalComputedTime
print "Average computation time per work: ",clusterAveragetimePerWork
print "Average memory usage per work: ",clusterAverageMemoryUsage
print "Average virtual memory usage per work: ",clusterAverageVirtualMemoryUsage

		









